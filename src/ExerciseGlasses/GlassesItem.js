import React, { Component } from "react";
import "../../src/index.css";

export default class GlassesItem extends Component {
  render() {
    return (
      <div
        className="col-6 "
        onClick={() => {
          this.props.handleGlass(
            this.props.listData.url,
            this.props.listData.name,
            this.props.listData.price
          );
        }}
      >
        <div className="card p-3 glass_item">
          <img
            src={this.props.listData.url}
            className="card-img-top"
            alt="..."
          />
          <div className="card-body">
            <h5 className="card-title">{this.props.listData.name}</h5>
            <p className="card-text">Price:{this.props.listData.price}$</p>
          </div>
        </div>
      </div>
    );
  }
}
