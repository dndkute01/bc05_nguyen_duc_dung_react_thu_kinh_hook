import React, { Component } from "react";
// import { dataGlasses } from "./dataGlasses";
import GlassesItem from "./GlassesItem";

export default class ListGlasses extends Component {
  render() {
    return (
      <div>
        <GlassesItem
          handleGlass={this.props.handleGlass}
          listData={this.props.data}
        />
      </div>
    );
  }
}
